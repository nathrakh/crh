## Interface: 11305
## Title: Character Runner Helper
## Notes: Automates some aspects of running characters to Razor Hill or Ratchet for honor pool expansion.
## Author: Nathrakh - Stalagg
## Version: 0.3

## SavedVariables: CRHHistory

CharacterRunnerHelper.lua
