local CRHFrame = CreateFrame("FRAME")
CRHFrame:RegisterEvent("PLAYER_LOGIN")
CRHFrame:RegisterEvent("PLAYER_ENTERING_WORLD")
CRHFrame:RegisterEvent("PARTY_INVITE_REQUEST")
CRHFrame:RegisterEvent("GOSSIP_SHOW")
CRHFrame:RegisterEvent("PLAYER_UNGHOST")
CRHFrame:RegisterEvent("PLAY_MOVIE")
CRHFrame:RegisterEvent("CINEMATIC_START")
CRHFrame:RegisterEvent("PLAYER_PVP_KILLS_CHANGED")
CRHFrame:RegisterEvent("ADDON_LOADED")

local prev_reset_epoch = 0

function in_razorhill_or_ratchet()
	local sub_zone = GetSubZoneText()
	return sub_zone == "Razor Hill" or sub_zone == "Ratchet"
end

function completed_15_hks()
	local week_hks, _ = GetPVPThisWeekStats()
	local today_hks, _ = GetPVPSessionStats()
	return week_hks + today_hks >= 15
end

-- This function assumes the honor reset time for US servers of 16:00 UTC Tuesday
function get_prev_honor_reset_epoch(now)
	local now_date = date("!*t", now)
	-- 1 is Sunday, so 3 is Tuesday
	local days_from_tuesday = 0
	if now_date.wday ~= 3 or now_date.hour < 16 then
		days_from_tuesday = (3 + now_date.wday) % 7 + 1
	end

	local seconds_from_last_reset = 86400*days_from_tuesday + 3600*(now_date.hour - 16) + 60*now_date.min + now_date.sec
	return now - seconds_from_last_reset
end

function have_seen_char(t, char_name)
	for _, tv in pairs(t) do
		for name, _ in pairs(tv) do
			if name == char_name then
				return true
			end
		end
	end
	return false
end

function remove_old_chars(t, reset_time)
	local j, n = 1, #t
	for i = 1, n do
		for _, ts in pairs(t[i]) do
			if ts >= reset_time then
				if i ~= j then
					t[j] = t[i]
					t[i] = nil
				end
				j = j + 1
			else
				t[i] = nil
			end
		end
	end
	return t
end

local function handle_events(self, event, arg1, ...)
	if event == "PLAYER_LOGIN" then
		hooksecurefunc("StaticPopup_Show", function(sType)
			if sType and sType == "DEATH" then
				if C_DeathInfo.GetSelfResurrectOptions() and #C_DeathInfo.GetSelfResurrectOptions() > 0 then return end
				local sub_zone = GetSubZoneText()
				if sub_zone == "Burning Blade Coven" or sub_zone == "The Merchant Coast" then
					C_Timer.After(0.2, function()
						local dialog = StaticPopup_Visible("DEATH")
						if dialog then
							StaticPopup_OnClick(_G[dialog], 1)
						end
					end)
				end
			elseif sType and sType == "XP_LOSS_NO_SICKNESS" and in_razorhill_or_ratchet() then
				AcceptXPLoss()
				StaticPopup_Hide("XP_LOSS_NO_SICKNESS")
			end
		end)
	elseif event == "PLAYER_ENTERING_WORLD" then
		CRHHistory.chars_this_week = remove_old_chars(CRHHistory.chars_this_week, prev_reset_epoch)
		local chars_this_week = #CRHHistory.chars_this_week
		print(("CharacterRunnerHelper: Ran %d characters this week and %d overall on this account."):format(chars_this_week, CRHHistory.total))

		local player_name = UnitName("player")
		if have_seen_char(CRHHistory.chars_this_week, player_name) then
			print("CharacterRunnerHelper: Warning: This character is already in the honor pool for this week.")
		end

		if UnitLevel("player") <= 5 and in_razorhill_or_ratchet() and not completed_15_hks() then
			SetPVP(1)
		end
	elseif event == "CINEMATIC_START" then
		if CinematicFrame.isRealCinematic then
			StopCinematic()
		elseif CanCancelScene() then
			CancelScene()
		end
		CRHFrame:UnregisterEvent("CINEMATIC_START")
	elseif event == "PLAY_MOVIE" then
		MovieFrame:Hide()
		CRHFrame:UnregisterEvent("PLAY_MOVIE")
	elseif event == "PARTY_INVITE_REQUEST" then
		if in_razorhill_or_ratchet() then
			AcceptGroup()
			StaticPopupDialogs["PARTY_INVITE"].inviteAccepted = 1
			StaticPopup_Hide("PARTY_INVITE")
		end
	elseif event == "GOSSIP_SHOW" then
		local unit_name, _ = UnitName("target")
		if unit_name == "Spirit Healer" and in_razorhill_or_ratchet() then
			SelectGossipOption(1)
		end
	elseif event == "PLAYER_UNGHOST" and in_razorhill_or_ratchet() then
		SetPVP(1)
	elseif event == "PLAYER_PVP_KILLS_CHANGED" then
		if UnitLevel("player") <= 5 and in_razorhill_or_ratchet() and completed_15_hks() then
			if IsInGroup() and not UnitIsGroupLeader("player") then
				LeaveParty()
			end

			local player_name = UnitName("player")
			if not have_seen_char(CRHHistory.chars_this_week, player_name) then
				local new_char = {[player_name] = GetServerTime()}
				table.insert(CRHHistory.chars_this_week, new_char)
				CRHHistory.total = CRHHistory.total + 1
			end
		end
	elseif event == "ADDON_LOADED" and arg1 == "CharacterRunnerHelper" then
		prev_reset_epoch = get_prev_honor_reset_epoch(GetServerTime())
		if CRHHistory == nil then
			CRHHistory = {}
			CRHHistory.chars_this_week = {}
			CRHHistory.total = 0
		end
	end
end

CRHFrame:SetScript("OnEvent", handle_events)
